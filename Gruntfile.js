module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-html-snapshot');

  grunt.initConfig({
    htmlSnapshot: {
      all: {
        options: {
          snapshotPath: 'snapshots/',
          sitePath: 'index.html', 
          fileNamePrefix: '',
          
          urls: ['#/home', '#/state2', '#/state1']
        }
      }
    }
  });

  grunt.registerTask('default', ['htmlSnapshot']);
};